/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.BDArchivos;
import Objetos.ObjAsientos;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class Asientos {
    BDArchivos bdArchivos= new BDArchivos();
    public void InsertarPersona(ArrayList<ObjAsientos> reservas){
        String datos="";
        for (int i=0;i<=reservas.size()-1;i++){
            String cedula= String.valueOf(reservas.get(i).getCedula());
            String asiento1= reservas.get(i).getAsiento1();
            String asiento2= reservas.get(i).getAsiento2();
            String asiento3= reservas.get(i).getAsiento3();
            String asiento4= reservas.get(i).getAsiento4();
            String asiento5= reservas.get(i).getAsiento5();
            String Precio= String.valueOf(reservas.get(i).getPrecio());
            String Fecha= reservas.get(i).getFecha();
            datos= cedula+","+asiento1+","+asiento2+","+asiento3+","+asiento4+","+asiento5+","+Precio+","+Fecha;
        }
        bdArchivos.InsertarEnArchivoPuestos(datos);
    }
    public ArrayList<ObjAsientos> LeerPuestos(){
        ArrayList<ObjAsientos> listaPersonas= bdArchivos.LeerDesdeArchivoPuestos();
        return listaPersonas;
    }
    
    public DefaultComboBoxModel cargarCedulas(){
        DefaultComboBoxModel modelo= new DefaultComboBoxModel();
        ArrayList<ObjAsientos> listaCedulas=bdArchivos.LeerDesdeArchivoPuestos();
        modelo.addElement("Seleccione una persona");
        for(ObjAsientos fila: listaCedulas){
            modelo.addElement(fila.getCedula());
        }
        return modelo;
    }
}
