/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.BDArchivos;
import Objetos.ObjPersonas;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Lukestas
 */
public class Personas {
    BDArchivos bdArchivos= new BDArchivos();
    public void InsertarPersona(ArrayList<ObjPersonas> Persona){
        String datos="";
        for (int i=0;i<=Persona.size()-1;i++){
            String cedula= String.valueOf(Persona.get(i).getCedula());
            String nombre= Persona.get(i).getNombre();
            String genero= Persona.get(i).getGenero();
            datos= cedula+","+nombre+","+genero;
        }
        bdArchivos.InsertarEnArchivoPersonas(datos);
    }
    
    public void EliminarcionArchivoPersonas(){
        bdArchivos.EliminarArchivoPersonas();
    }
    public DefaultComboBoxModel cargarCedulas(){
        DefaultComboBoxModel modelo= new DefaultComboBoxModel();
        ArrayList<ObjPersonas> listaCedulas=bdArchivos.LeerDesdeArchivoPersonas();
        modelo.addElement("Seleccione una persona");
        for(ObjPersonas fila: listaCedulas){
            modelo.addElement(fila.getCedula());
        }
        return modelo;
    }
    
    public ArrayList<ObjPersonas> LeerPersonas(){
        ArrayList<ObjPersonas> listaPersonas= bdArchivos.LeerDesdeArchivoPersonas();
        return listaPersonas;
    }
}
