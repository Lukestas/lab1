/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjAsientos {
    private Integer Cedula;
    private String asiento1;
    private String asiento2;
    private String asiento3;
    private String asiento4;
    private String asiento5;
    private Integer Precio;
    private String Fecha;

    public ObjAsientos(Integer Cedula, String asiento1, String asiento2, String asiento3, String asiento4, String asiento5, Integer Precio, String Fecha) {
        this.Cedula = Cedula;
        this.asiento1 = asiento1;
        this.asiento2 = asiento2;
        this.asiento3 = asiento3;
        this.asiento4 = asiento4;
        this.asiento5 = asiento5;
        this.Precio = Precio;
        this.Fecha = Fecha;
    }
    
    public static ArrayList listaAsientos=new ArrayList<>();

    /**
     * @return the Cedula
     */
    public Integer getCedula() {
        return Cedula;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(Integer Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @return the asiento1
     */
    public String getAsiento1() {
        return asiento1;
    }

    /**
     * @param asiento1 the asiento1 to set
     */
    public void setAsiento1(String asiento1) {
        this.asiento1 = asiento1;
    }

    /**
     * @return the asiento2
     */
    public String getAsiento2() {
        return asiento2;
    }

    /**
     * @param asiento2 the asiento2 to set
     */
    public void setAsiento2(String asiento2) {
        this.asiento2 = asiento2;
    }

    /**
     * @return the asiento3
     */
    public String getAsiento3() {
        return asiento3;
    }

    /**
     * @param asiento3 the asiento3 to set
     */
    public void setAsiento3(String asiento3) {
        this.asiento3 = asiento3;
    }

    /**
     * @return the asiento4
     */
    public String getAsiento4() {
        return asiento4;
    }

    /**
     * @param asiento4 the asiento4 to set
     */
    public void setAsiento4(String asiento4) {
        this.asiento4 = asiento4;
    }

    /**
     * @return the asiento5
     */
    public String getAsiento5() {
        return asiento5;
    }

    /**
     * @param asiento5 the asiento5 to set
     */
    public void setAsiento5(String asiento5) {
        this.asiento5 = asiento5;
    }

    /**
     * @return the Precio
     */
    public Integer getPrecio() {
        return Precio;
    }

    /**
     * @param Precio the Precio to set
     */
    public void setPrecio(Integer Precio) {
        this.Precio = Precio;
    }

    /**
     * @return the Fecha
     */
    public String getFecha() {
        return Fecha;
    }

    /**
     * @param Fecha the Fecha to set
     */
    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }
    
    
}
