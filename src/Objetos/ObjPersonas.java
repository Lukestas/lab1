/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Lukestas
 */
public class ObjPersonas {
    private Integer Cedula;
    private String Nombre;
    private String Genero;

    public ObjPersonas(Integer Cedula, String Nombre, String Genero) {
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Genero = Genero;
    }
    
    public static ArrayList listaPersonas= new ArrayList<>();

    /**
     * @return the Cedula
     */
    public Integer getCedula() {
        return Cedula;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(Integer Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Genero
     */
    public String getGenero() {
        return Genero;
    }

    /**
     * @param Genero the Genero to set
     */
    public void setGenero(String Genero) {
        this.Genero = Genero;
    }
    
    
}
