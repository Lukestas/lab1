/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objetos.ObjAsientos;
import Objetos.ObjPersonas;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Lukestas
 */
public class BDArchivos {
    
    public void InsertarEnArchivoPuestos(String datos){
        try {
            File archivo = new File("puestos.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datos + "\r\n");
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al guardar la persona");
        }
    }

    public void InsertarEnArchivoPersonas(String datos) {
        try {
            File archivo = new File("personas.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datos + "\r\n");
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al guardar la persona");
        }
    }

    public ArrayList<ObjPersonas> LeerDesdeArchivoPersonas() {
        try {
            File archivo = new File("personas.txt");
            if (archivo.exists()) {
                BufferedReader archi = new BufferedReader(new FileReader(archivo));
                while (archi.ready()) {
                    String[] separar = new String[3];
                    separar = archi.readLine().split(",");
                    ObjPersonas.listaPersonas.add(new ObjPersonas(Integer.valueOf(separar[0]), separar[1], separar[2]));
                }
                archi.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer archivo personas");

        }
        return ObjPersonas.listaPersonas;
    }
    
    public ArrayList<ObjAsientos> LeerDesdeArchivoPuestos() {
        try {
            File archivo = new File("puestos.txt");
            if (archivo.exists()) {
                BufferedReader archi = new BufferedReader(new FileReader(archivo));
                while (archi.ready()) {
                    String[] separar = new String[3];
                    separar = archi.readLine().split(",");
                    ObjAsientos.listaAsientos.add(new ObjAsientos(Integer.valueOf(separar[0]), separar[1], separar[2],separar[3],separar[4],separar[5],Integer.valueOf(separar[6]),separar[7]));
                }
                archi.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer archivo personas");

        }
        return ObjAsientos.listaAsientos;
    }

    public void EliminarArchivoPersonas() {
        try {
            File archivo = new File("personas.txt");
            if (archivo.exists()) {
                if (archivo.delete()) {
                    System.out.println("se borro");
                } else {
                    System.out.println("no se borro");
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al modificar el archivo");
        }
    }
}
