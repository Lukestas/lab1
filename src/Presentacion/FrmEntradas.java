/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.util.*;
import negocio.Asientos;
import Objetos.ObjAsientos;
import Objetos.ObjPersonas;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.table.DefaultTableModel;
import negocio.Personas;

/**
 *
 * @author Lukestas
 */
public class FrmEntradas extends javax.swing.JDialog {

    Personas personita = new Personas();
    JButton[] Lista = new JButton[72];
    Asientos sentarse = new Asientos();
    Integer cont = 0;

    /**
     * Creates new form sentarse
     */
    public FrmEntradas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Deteccion();
        asignacion();
        AsientosOcupados();
        mostrarTabla();
        llenarComprados();
        llenarCombo();
    }

    public void Deteccion() {
        A1.addActionListener(new BotonPulsadoListener(A1));
        A2.addActionListener(new BotonPulsadoListener(A2));
        A3.addActionListener(new BotonPulsadoListener(A3));
        A4.addActionListener(new BotonPulsadoListener(A4));
        A5.addActionListener(new BotonPulsadoListener(A5));
        A6.addActionListener(new BotonPulsadoListener(A6));
        A7.addActionListener(new BotonPulsadoListener(A7));
        A8.addActionListener(new BotonPulsadoListener(A8));

        B1.addActionListener(new BotonPulsadoListener(B1));
        B2.addActionListener(new BotonPulsadoListener(B2));
        B3.addActionListener(new BotonPulsadoListener(B3));
        B4.addActionListener(new BotonPulsadoListener(B4));
        B5.addActionListener(new BotonPulsadoListener(B5));
        B6.addActionListener(new BotonPulsadoListener(B6));
        B7.addActionListener(new BotonPulsadoListener(B7));
        B8.addActionListener(new BotonPulsadoListener(B8));

        C1.addActionListener(new BotonPulsadoListener(C1));
        C2.addActionListener(new BotonPulsadoListener(C2));
        C3.addActionListener(new BotonPulsadoListener(C3));
        C4.addActionListener(new BotonPulsadoListener(C4));
        C5.addActionListener(new BotonPulsadoListener(C5));
        C6.addActionListener(new BotonPulsadoListener(C6));
        C7.addActionListener(new BotonPulsadoListener(C7));
        C8.addActionListener(new BotonPulsadoListener(C8));

        D1.addActionListener(new BotonPulsadoListener(D1));
        D2.addActionListener(new BotonPulsadoListener(D2));
        D3.addActionListener(new BotonPulsadoListener(D3));
        D4.addActionListener(new BotonPulsadoListener(D4));
        D5.addActionListener(new BotonPulsadoListener(D5));
        D6.addActionListener(new BotonPulsadoListener(D6));
        D7.addActionListener(new BotonPulsadoListener(D7));
        D8.addActionListener(new BotonPulsadoListener(D8));

        E1.addActionListener(new BotonPulsadoListener(E1));
        E2.addActionListener(new BotonPulsadoListener(E2));
        E3.addActionListener(new BotonPulsadoListener(E3));
        E4.addActionListener(new BotonPulsadoListener(E4));
        E5.addActionListener(new BotonPulsadoListener(E5));
        E6.addActionListener(new BotonPulsadoListener(E6));
        E7.addActionListener(new BotonPulsadoListener(E7));
        E8.addActionListener(new BotonPulsadoListener(E8));

        F1.addActionListener(new BotonPulsadoListener(F1));
        F2.addActionListener(new BotonPulsadoListener(F2));
        F3.addActionListener(new BotonPulsadoListener(F3));
        F4.addActionListener(new BotonPulsadoListener(F4));
        F5.addActionListener(new BotonPulsadoListener(F5));
        F6.addActionListener(new BotonPulsadoListener(F6));
        F7.addActionListener(new BotonPulsadoListener(F7));
        F8.addActionListener(new BotonPulsadoListener(F8));

        G1.addActionListener(new BotonPulsadoListener(G1));
        G2.addActionListener(new BotonPulsadoListener(G2));
        G3.addActionListener(new BotonPulsadoListener(G3));
        G4.addActionListener(new BotonPulsadoListener(G4));
        G5.addActionListener(new BotonPulsadoListener(G5));
        G6.addActionListener(new BotonPulsadoListener(G6));
        G7.addActionListener(new BotonPulsadoListener(G7));
        G8.addActionListener(new BotonPulsadoListener(G8));

        H1.addActionListener(new BotonPulsadoListener(H1));
        H2.addActionListener(new BotonPulsadoListener(H2));
        H3.addActionListener(new BotonPulsadoListener(H3));
        H4.addActionListener(new BotonPulsadoListener(H4));
        H5.addActionListener(new BotonPulsadoListener(H5));
        H6.addActionListener(new BotonPulsadoListener(H6));
        H7.addActionListener(new BotonPulsadoListener(H7));
        H8.addActionListener(new BotonPulsadoListener(H8));

        I1.addActionListener(new BotonPulsadoListener(I1));
        I2.addActionListener(new BotonPulsadoListener(I2));
        I3.addActionListener(new BotonPulsadoListener(I3));
        I4.addActionListener(new BotonPulsadoListener(I4));
        I5.addActionListener(new BotonPulsadoListener(I5));
        I6.addActionListener(new BotonPulsadoListener(I6));
        I7.addActionListener(new BotonPulsadoListener(I7));
        I8.addActionListener(new BotonPulsadoListener(I8));
    }

    public void asignacion() {
        Lista[0] = A1;
        Lista[1] = A2;
        Lista[2] = A3;
        Lista[3] = A4;
        Lista[4] = A5;
        Lista[5] = A6;
        Lista[6] = A7;
        Lista[7] = A8;
        Lista[8] = B1;
        Lista[9] = B2;
        Lista[10] = B3;
        Lista[11] = B4;
        Lista[12] = B5;
        Lista[13] = B6;
        Lista[14] = B7;
        Lista[15] = B8;
        Lista[16] = C1;
        Lista[17] = C2;
        Lista[18] = C3;
        Lista[19] = C4;
        Lista[20] = C5;
        Lista[21] = C6;
        Lista[22] = C7;
        Lista[23] = C8;
        Lista[24] = D1;
        Lista[25] = D2;
        Lista[26] = D3;
        Lista[27] = D4;
        Lista[28] = D5;
        Lista[29] = D6;
        Lista[30] = D7;
        Lista[31] = D8;
        Lista[32] = E1;
        Lista[33] = E2;
        Lista[34] = E3;
        Lista[35] = E4;
        Lista[36] = E5;
        Lista[37] = E6;
        Lista[38] = E7;
        Lista[39] = E8;
        Lista[40] = F1;
        Lista[41] = F2;
        Lista[42] = F3;
        Lista[43] = F4;
        Lista[44] = F5;
        Lista[45] = F6;
        Lista[46] = F7;
        Lista[47] = F8;
        Lista[48] = G1;
        Lista[49] = G2;
        Lista[50] = G3;
        Lista[51] = G4;
        Lista[52] = G5;
        Lista[53] = G6;
        Lista[54] = G7;
        Lista[55] = G8;
        Lista[56] = H1;
        Lista[57] = H2;
        Lista[58] = H3;
        Lista[59] = H4;
        Lista[60] = H5;
        Lista[61] = H6;
        Lista[62] = H7;
        Lista[63] = H8;
        Lista[64] = I1;
        Lista[65] = I2;
        Lista[66] = I3;
        Lista[67] = I4;
        Lista[68] = I5;
        Lista[69] = I6;
        Lista[70] = I7;
        Lista[71] = I8;
    }

    public void AsientosOcupados() {
        try {
            ArrayList<Integer> Sillas;
            Sillas = new ArrayList<>();
            Set<Integer> generados = new HashSet<>();
            for (int i = 0; i <= 25; i++) {
                int aleatorio = -1;
                boolean generado = false;
                while (!generado) {
                    Random knk = new Random();
                    int posible = (int) (knk.nextInt(71));
                    if (!generados.contains(posible)) {
                        generados.add(posible);
                        aleatorio = posible;
                        Lista[posible].setIcon(btnOcupado.getIcon());
                        generado = true;
                    }
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en el programa");
        }
    }

    private class BotonPulsadoListener implements ActionListener {

        private JButton boton;

        public BotonPulsadoListener(JButton boton) {
            this.boton = boton;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String Disponible = btnDisponible.getIcon().toString();
            String Ocupado = btnOcupado.getIcon().toString();
            String Seleccionado = btnSeleccionado.getIcon().toString();
            if (cont == 5) {
                if (boton.getIcon().toString().equals(Seleccionado)) {
                    boton.setIcon(btnDisponible.getIcon());
                    cont--;
                } else {
                    JOptionPane.showMessageDialog(null, "Limite de asientos seleccionados");
                }
            } else {
                if (boton.getIcon().toString().equals(Disponible)) {
                    boton.setIcon(btnSeleccionado.getIcon());
                    cont++;
                } else if (boton.getIcon().toString().equals(Seleccionado)) {
                    boton.setIcon(btnDisponible.getIcon());
                    cont--;
                } else if (boton.getIcon().toString().equals(Ocupado)) {
                    JOptionPane.showMessageDialog(null, "El asiento que selecciono se encuentra ocupado");
                }
            }

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Asientos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        A1 = new javax.swing.JButton();
        A2 = new javax.swing.JButton();
        A3 = new javax.swing.JButton();
        A4 = new javax.swing.JButton();
        A5 = new javax.swing.JButton();
        A6 = new javax.swing.JButton();
        A7 = new javax.swing.JButton();
        A8 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();
        B5 = new javax.swing.JButton();
        B6 = new javax.swing.JButton();
        B7 = new javax.swing.JButton();
        B8 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        C1 = new javax.swing.JButton();
        C2 = new javax.swing.JButton();
        C3 = new javax.swing.JButton();
        C4 = new javax.swing.JButton();
        C5 = new javax.swing.JButton();
        C6 = new javax.swing.JButton();
        C7 = new javax.swing.JButton();
        C8 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        D1 = new javax.swing.JButton();
        D2 = new javax.swing.JButton();
        D3 = new javax.swing.JButton();
        D4 = new javax.swing.JButton();
        D5 = new javax.swing.JButton();
        D6 = new javax.swing.JButton();
        D7 = new javax.swing.JButton();
        D8 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        E7 = new javax.swing.JButton();
        E6 = new javax.swing.JButton();
        E4 = new javax.swing.JButton();
        E5 = new javax.swing.JButton();
        E8 = new javax.swing.JButton();
        E3 = new javax.swing.JButton();
        E2 = new javax.swing.JButton();
        E1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        F8 = new javax.swing.JButton();
        F7 = new javax.swing.JButton();
        F6 = new javax.swing.JButton();
        F5 = new javax.swing.JButton();
        F4 = new javax.swing.JButton();
        F3 = new javax.swing.JButton();
        F2 = new javax.swing.JButton();
        F1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        G8 = new javax.swing.JButton();
        G7 = new javax.swing.JButton();
        G6 = new javax.swing.JButton();
        G5 = new javax.swing.JButton();
        G4 = new javax.swing.JButton();
        G3 = new javax.swing.JButton();
        G2 = new javax.swing.JButton();
        G1 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        H8 = new javax.swing.JButton();
        H7 = new javax.swing.JButton();
        H6 = new javax.swing.JButton();
        H5 = new javax.swing.JButton();
        H4 = new javax.swing.JButton();
        H3 = new javax.swing.JButton();
        H2 = new javax.swing.JButton();
        H1 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        I1 = new javax.swing.JButton();
        I2 = new javax.swing.JButton();
        I3 = new javax.swing.JButton();
        I4 = new javax.swing.JButton();
        I5 = new javax.swing.JButton();
        I6 = new javax.swing.JButton();
        I7 = new javax.swing.JButton();
        I8 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        btnOcupado = new javax.swing.JButton();
        btnDisponible = new javax.swing.JButton();
        btnSeleccionado = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtCedula = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbCine = new javax.swing.JTable();
        cbCobrados = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel1.setText("Pantalla");

        A1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A1.setText("A1");

        A2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A2.setText("A2");

        A3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A3.setText("A3");

        A4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A4.setText("A4");

        A5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A5.setText("A5");

        A6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A6.setText("A6");

        A7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A7.setText("A7");

        A8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        A8.setText("A8");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(A1)
                    .addComponent(A2)
                    .addComponent(A3)
                    .addComponent(A4)
                    .addComponent(A5)
                    .addComponent(A6)
                    .addComponent(A7)
                    .addComponent(A8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(A8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(A1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        B1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B1.setText("B1");

        B2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B2.setText("B2");

        B3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B3.setText("B3");

        B4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B4.setText("B4");

        B5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B5.setText("B5");

        B6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B6.setText("B6");

        B7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B7.setText("B7");

        B8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        B8.setText("B8");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(B8)
                    .addComponent(B1)
                    .addComponent(B2)
                    .addComponent(B3)
                    .addComponent(B4)
                    .addComponent(B5)
                    .addComponent(B6)
                    .addComponent(B7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(B8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(B1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        C1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C1.setText("C1");

        C2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C2.setText("C2");

        C3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C3.setText("C3");

        C4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C4.setText("C4");

        C5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C5.setText("C5");

        C6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C6.setText("C6");

        C7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C7.setText("C7");

        C8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        C8.setText("C8");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(C8)
                    .addComponent(C1)
                    .addComponent(C2)
                    .addComponent(C3)
                    .addComponent(C4)
                    .addComponent(C5)
                    .addComponent(C6)
                    .addComponent(C7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(C8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(C1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        D1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D1.setText("D1");

        D2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D2.setText("D2");

        D3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D3.setText("D3");

        D4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D4.setText("D4");

        D5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D5.setText("D5");

        D6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D6.setText("D6");

        D7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D7.setText("D7");

        D8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        D8.setText("D8");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(D1)
                    .addComponent(D2)
                    .addComponent(D3)
                    .addComponent(D4)
                    .addComponent(D5)
                    .addComponent(D6)
                    .addComponent(D7)
                    .addComponent(D8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(D8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(D1))
        );

        E7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E7.setText("E7");

        E6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E6.setText("E6");

        E4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E4.setText("E4");

        E5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E5.setText("E5");

        E8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E8.setText("E8");

        E3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E3.setText("E3");

        E2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E2.setText("E2");

        E1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        E1.setText("E1");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(E1)
                    .addComponent(E2)
                    .addComponent(E3)
                    .addComponent(E4)
                    .addComponent(E5)
                    .addComponent(E6)
                    .addComponent(E7)
                    .addComponent(E8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(E8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        F8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F8.setText("F8");

        F7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F7.setText("F7");

        F6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F6.setText("F6");

        F5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F5.setText("F5");

        F4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F4.setText("F4");

        F3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F3.setText("F3");

        F2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F2.setText("F2");

        F1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        F1.setText("F1");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(F8)
                    .addComponent(F7)
                    .addComponent(F6)
                    .addComponent(F4)
                    .addComponent(F5)
                    .addComponent(F3)
                    .addComponent(F2)
                    .addComponent(F1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(F8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(F1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        G8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G8.setText("G8");

        G7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G7.setText("G7");

        G6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G6.setText("G6");

        G5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G5.setText("G5");

        G4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G4.setText("G4");

        G3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G3.setText("G3");

        G2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G2.setText("G2");

        G1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        G1.setText("G1");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(G8)
                    .addComponent(G7)
                    .addComponent(G6)
                    .addComponent(G5)
                    .addComponent(G4)
                    .addComponent(G3)
                    .addComponent(G2)
                    .addComponent(G1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(G8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(G1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        H8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H8.setText("H8");

        H7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H7.setText("H7");

        H6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H6.setText("H6");

        H5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H5.setText("H5");

        H4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H4.setText("H4");

        H3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H3.setText("H3");

        H2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H2.setText("H2");

        H1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        H1.setText("H1");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(H8)
                    .addComponent(H7)
                    .addComponent(H6)
                    .addComponent(H5)
                    .addComponent(H4)
                    .addComponent(H3)
                    .addComponent(H2)
                    .addComponent(H1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(H8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(H1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        I1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I1.setText("I1");

        I2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I2.setText("I2");

        I3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I3.setText("I3");

        I4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I4.setText("I4");

        I5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I5.setText("I5");

        I6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I6.setText("I6");

        I7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I7.setText("I7");

        I8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        I8.setText("I8");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(I1)
                    .addComponent(I2)
                    .addComponent(I3)
                    .addComponent(I4)
                    .addComponent(I5)
                    .addComponent(I6)
                    .addComponent(I7)
                    .addComponent(I8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(I8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(I1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout AsientosLayout = new javax.swing.GroupLayout(Asientos);
        Asientos.setLayout(AsientosLayout);
        AsientosLayout.setHorizontalGroup(
            AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AsientosLayout.createSequentialGroup()
                .addGroup(AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AsientosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(AsientosLayout.createSequentialGroup()
                        .addGap(383, 383, 383)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        AsientosLayout.setVerticalGroup(
            AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AsientosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(AsientosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnOcupado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca1.jpg"))); // NOI18N
        btnOcupado.setText("Ocupado");

        btnDisponible.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca0.jpg"))); // NOI18N
        btnDisponible.setText("Disponible");

        btnSeleccionado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Butaca2_1.jpg"))); // NOI18N
        btnSeleccionado.setText("Seleccionado");

        jLabel3.setText("₡5000 asientos, filas 1-4");

        jLabel4.setText("₡3000 asientos, filas 5-8");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnSeleccionado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDisponible, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnOcupado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnOcupado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDisponible)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSeleccionado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("Comprar Entradas");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txtCedula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaKeyTyped(evt);
            }
        });

        jLabel2.setText("Ingrese su cedula:");

        tbCine.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Cedula", "1° Asiento", "2° Asiento", "3° Asiento", "4° Asiento", "5° Asiento", "Pagó", "Fecha", "Nombre", "Genero"
            }
        ));
        jScrollPane1.setViewportView(tbCine);

        cbCobrados.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCobradosItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Asientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbCobrados, 0, 112, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Asientos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(cbCobrados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void llenarCombo() {
        ObjAsientos.listaAsientos = new ArrayList<>();
        cbCobrados.setModel(sentarse.cargarCedulas());
    }

    public void llenarComprados() {
        ObjAsientos.listaAsientos = new ArrayList<>();
        ArrayList<ObjAsientos> listaDatos = sentarse.LeerPuestos();
        Object[] sillita = new Object[5];
        for (ObjAsientos fila : listaDatos) {
            sillita[0] = fila.getAsiento1();
            sillita[1] = fila.getAsiento2();
            sillita[2] = fila.getAsiento3();
            sillita[3] = fila.getAsiento4();
            sillita[4] = fila.getAsiento5();
            for (int i = 0; i < Lista.length; i++) {
                if (sillita[0].equals(Lista[i].getText())) {
                    Lista[i].setIcon(btnOcupado.getIcon());
                }
                if (sillita[1].equals(Lista[i].getText())) {
                    Lista[i].setIcon(btnOcupado.getIcon());
                }
                if (sillita[2].equals(Lista[i].getText())) {
                    Lista[i].setIcon(btnOcupado.getIcon());
                }
                if (sillita[3].equals(Lista[i].getText())) {
                    Lista[i].setIcon(btnOcupado.getIcon());
                }
                if (sillita[4].equals(Lista[i].getText())) {
                    Lista[i].setIcon(btnOcupado.getIcon());
                }

            }
        }

    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (validar()) {
            if (verificado()) {
                String PaComprar = btnSeleccionado.getIcon().toString();
                String[] asientos = new String[5];
                int contador = 0;
                int costo = 0;
                for (int i = 0; i < Lista.length; i++) {
                    if (Lista[i].getIcon().toString().equals(PaComprar)) {
                        asientos[contador] = Lista[i].getText();
                        Lista[i].setIcon(btnOcupado.getIcon());
                        contador++;
                        cont--;
                        int numero = Integer.valueOf(Lista[i].getText().charAt(1));
                        if (numero >= 4) {
                            costo += 5000;
                        } else {
                            costo += 3000;
                        }
                    }
                }
                Integer Cedula = Integer.valueOf(txtCedula.getText());
                String asiento1 = asientos[0];
                String asiento2 = asientos[1];
                String asiento3 = asientos[2];
                String asiento4 = asientos[3];
                String asiento5 = asientos[4];
                Integer precio = costo;
                String Fecha = FechaHora();
                ObjAsientos.listaAsientos = new ArrayList<>();
                ObjAsientos.listaAsientos.add(new ObjAsientos(Cedula, asiento1, asiento2, asiento3, asiento4, asiento5, precio, Fecha));
                sentarse.InsertarPersona(ObjAsientos.listaAsientos);
                mostrarTabla();
                llenarCombo();
            } else {
                JOptionPane.showMessageDialog(null, "Esta persona ya realizo un registro");
            }
        } else {
            JOptionPane.showMessageDialog(null, "La cedula no éxiste en el registro");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public void mostrarTabla() {
        ObjAsientos.listaAsientos = new ArrayList<>();
        DefaultTableModel modelo = (DefaultTableModel) tbCine.getModel();
        modelo.setRowCount(0);
        ArrayList<ObjAsientos> listaDatos = sentarse.LeerPuestos();
        Object[] arreglo = new Object[8];
        for (ObjAsientos fila : listaDatos) {
            arreglo[0] = fila.getCedula();
            arreglo[1] = fila.getAsiento1();
            arreglo[2] = fila.getAsiento2();
            arreglo[3] = fila.getAsiento3();
            arreglo[4] = fila.getAsiento4();
            arreglo[5] = fila.getAsiento5();
            arreglo[6] = fila.getPrecio();
            arreglo[7] = fila.getFecha();
            modelo.addRow(arreglo);
        }
    }

    public boolean validar() {
        boolean continuar = false;
        ObjPersonas.listaPersonas = new ArrayList<>();
        ArrayList<ObjPersonas> datos = personita.LeerPersonas();
        for (ObjPersonas fila : datos) {
            if (txtCedula.getText().equals(String.valueOf(fila.getCedula()))) {
                continuar = true;
            }
        }
        return continuar;
    }

    public boolean verificado() {
        boolean continuar = true;
        ObjAsientos.listaAsientos = new ArrayList<>();
        ArrayList<ObjAsientos> datos = sentarse.LeerPuestos();
        for (ObjAsientos fila : datos) {
            if (txtCedula.getText().equals(String.valueOf(fila.getCedula()))) {
                continuar = false;
            }
        }
        return continuar;
    }

    public String FechaHora() {
        DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a");
        System.out.println(fecha.format(LocalDateTime.now()));
        return fecha.format(LocalDateTime.now());
    }

    private void txtCedulaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Ingresar unicamente numeros");
        }
    }//GEN-LAST:event_txtCedulaKeyTyped

    private void cbCobradosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCobradosItemStateChanged
        if (!cbCobrados.getSelectedItem().equals("Seleccione una persona")) {
            ObjAsientos.listaAsientos = new ArrayList<>();
            ObjPersonas.listaPersonas= new ArrayList<>();
            DefaultTableModel modelo = (DefaultTableModel) tbCine.getModel();
            modelo.setRowCount(0);
            ArrayList<ObjAsientos> listaDatos = sentarse.LeerPuestos();
            ArrayList<ObjPersonas> listaPersonalidad = personita.LeerPersonas();
            Object[] arreglo = new Object[10];
            for (ObjPersonas filasss : listaPersonalidad) {
                Integer Cedula1=filasss.getCedula();
                for (ObjAsientos fila : listaDatos) {
                    Integer Cedula2=fila.getCedula();
                    if (Cedula1 == Cedula2) {
                        if (fila.getCedula().equals(cbCobrados.getSelectedItem())) {
                            arreglo[0] = fila.getCedula();
                            arreglo[1] = fila.getAsiento1();
                            arreglo[2] = fila.getAsiento2();
                            arreglo[3] = fila.getAsiento3();
                            arreglo[4] = fila.getAsiento4();
                            arreglo[5] = fila.getAsiento5();
                            arreglo[6] = fila.getPrecio();
                            arreglo[7] = fila.getFecha();
                            arreglo[8] = filasss.getNombre();
                            arreglo[9] = filasss.getGenero();
                            modelo.addRow(arreglo);
                        }
                    }
                }
            }
        } else {
            mostrarTabla();
        }
    }//GEN-LAST:event_cbCobradosItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmEntradas dialog = new FrmEntradas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton A1;
    private javax.swing.JButton A2;
    private javax.swing.JButton A3;
    private javax.swing.JButton A4;
    private javax.swing.JButton A5;
    private javax.swing.JButton A6;
    private javax.swing.JButton A7;
    private javax.swing.JButton A8;
    private javax.swing.JPanel Asientos;
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JButton C1;
    private javax.swing.JButton C2;
    private javax.swing.JButton C3;
    private javax.swing.JButton C4;
    private javax.swing.JButton C5;
    private javax.swing.JButton C6;
    private javax.swing.JButton C7;
    private javax.swing.JButton C8;
    private javax.swing.JButton D1;
    private javax.swing.JButton D2;
    private javax.swing.JButton D3;
    private javax.swing.JButton D4;
    private javax.swing.JButton D5;
    private javax.swing.JButton D6;
    private javax.swing.JButton D7;
    private javax.swing.JButton D8;
    private javax.swing.JButton E1;
    private javax.swing.JButton E2;
    private javax.swing.JButton E3;
    private javax.swing.JButton E4;
    private javax.swing.JButton E5;
    private javax.swing.JButton E6;
    private javax.swing.JButton E7;
    private javax.swing.JButton E8;
    private javax.swing.JButton F1;
    private javax.swing.JButton F2;
    private javax.swing.JButton F3;
    private javax.swing.JButton F4;
    private javax.swing.JButton F5;
    private javax.swing.JButton F6;
    private javax.swing.JButton F7;
    private javax.swing.JButton F8;
    private javax.swing.JButton G1;
    private javax.swing.JButton G2;
    private javax.swing.JButton G3;
    private javax.swing.JButton G4;
    private javax.swing.JButton G5;
    private javax.swing.JButton G6;
    private javax.swing.JButton G7;
    private javax.swing.JButton G8;
    private javax.swing.JButton H1;
    private javax.swing.JButton H2;
    private javax.swing.JButton H3;
    private javax.swing.JButton H4;
    private javax.swing.JButton H5;
    private javax.swing.JButton H6;
    private javax.swing.JButton H7;
    private javax.swing.JButton H8;
    private javax.swing.JButton I1;
    private javax.swing.JButton I2;
    private javax.swing.JButton I3;
    private javax.swing.JButton I4;
    private javax.swing.JButton I5;
    private javax.swing.JButton I6;
    private javax.swing.JButton I7;
    private javax.swing.JButton I8;
    private javax.swing.JButton btnDisponible;
    private javax.swing.JButton btnOcupado;
    private javax.swing.JButton btnSeleccionado;
    private javax.swing.JComboBox<String> cbCobrados;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbCine;
    private javax.swing.JTextField txtCedula;
    // End of variables declaration//GEN-END:variables
}
